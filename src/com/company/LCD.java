package com.company;

public class LCD {

    String status;
    int volume;
    int brightness;
    String[] tipeCable;
    String cable;

    public LCD(){
        this.status = status;
        this.volume = volume;
        this.brightness = brightness;
        this.cable = "VGA";
        tipeCable = new String[]{"VGA","DVI", "HDMI", "DISPLAY PORT"};
    }

    public void turnOff (){
        this.status = "Off";
    }

    public void turnOn (){
        this.status = "On";
    }

    public void Freeze(){
        this.status = "Freeze";
    }

    public void volumeUp(){
        this.volume += 1;
    }

    public void volumeDown(){
        this.volume -= 1;
    }

    public void setVolume(int volume){
        this.volume = volume;
    }

    public void brightnessUp (int brightness){
        this.brightness += brightness;
    }

    public void brightnessDown (int brightness){
        this.brightness -= brightness;
    }

    public void setBrightness(int brightness){
        this.brightness = brightness;
    }

    public void setCable(String cable){
        this.cable = cable;
    }

    public void cableUp(){
        for (int i = 0; i < tipeCable.length; i++){
            if(cable.equals(tipeCable[i])){
                if(i==3){
                    this.cable = tipeCable[0];
                } else {
                    this.cable = tipeCable[i+1];
                } break;
            }
        }
    }

    public void cableDown(){
        for (int i = 0; i < tipeCable.length; i++){
            if(this.cable.equals(tipeCable[i])){
                if(i==0){
                    this.cable = tipeCable[3];
                } else {
                    this.cable = tipeCable[i-1];
                } break;
            }
        }
    }

    public void displayMassage(){
        System.out.println("-------------- LCD -------------- ");
        System.out.println("Status LCD saat ini     : " + this.status);
        System.out.println("Volume LCD saat ini     : " + this.volume);
        System.out.println("Brightness saat ini     : " + this.brightness);
        System.out.println("Cable yang digunakan    : " + this.cable);
    }

}
